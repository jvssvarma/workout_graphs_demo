require "rails_helper"

RSpec.feature "Creating a Homepage" do
    scenario  do
        visit '/'
        
        expect(page).to have_link('Home')
        expect(page).to have_link('Workouts Space')
        expect(page).to have_content('Workout Lounge')
        expect(page).to have_content('See your Workout')
    end
end